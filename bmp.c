#include "bmp.h"
#include <stdlib.h>

#pragma pack(push, 1)
struct header {
	uint16_t type;
	uint32_t file_size;
	uint16_t reserved_1, reserved_2;
	uint32_t offset, header_size, width, height;
};
#pragma pack(pop)

static void fclose_checked(FILE* const what) {
	if (what != NULL) fclose(what);
}

enum bmp_status bmp_load(FILE* const in, struct image* const dest) {
	enum bmp_status err = bmp_ok;
	struct header header = {0};
	if (fread(&header, sizeof header, 1, in) != 1) CLEANUP_WITH(bmp_fmt);
	dest->width = (size_t)header.width;
	dest->height = (size_t)header.height;
	dest->data = malloc(sizeof(struct pixel) * dest->width * dest->height);
	if (dest->data == NULL) CLEANUP_WITH(bmp_oom);
	if (fseek(in, header.offset, SEEK_SET) != 0) CLEANUP_WITH(bmp_fmt);
	uint8_t pad[3] = {0};
	for (size_t y = dest->height; y-- != 0; ) {
		struct pixel* const line = dest->data + y * dest->width;
		size_t const length = sizeof(struct pixel) * dest->width;
		if (fread(line, length, 1, in) != 1) CLEANUP_WITH(bmp_fmt);
		if (dest->width % 4 != 0) {
			if (fread(pad, dest->width % 4, 1, in) != 1) {
				CLEANUP_WITH(bmp_fmt);
			}
		}
	}
cleanup:
	if (err != bmp_ok) free(dest->data);
	return err;
}

enum bmp_status bmp_save(FILE* const io, struct image const src) {
	enum bmp_status err = bmp_ok;
	struct header header = {0};
	if (fread(&header, sizeof header, 1, io) != 1) CLEANUP_WITH(bmp_fmt);
	if (fseek(io, header.offset, SEEK_SET) != 0) CLEANUP_WITH(bmp_fmt);
	struct pixel pad = {0};
	for (size_t y = src.height; y-- != 0; ) {
		struct pixel const* const line = src.data + y * src.width;
		size_t const length = sizeof(struct pixel) * src.width;
		if (fwrite(line, length, 1, io) != 1) CLEANUP_WITH(bmp_fmt);
		if (src.width % 4 != 0) {
			if (fwrite(&pad, src.width % 4, 1, io) != 1) {
				CLEANUP_WITH(bmp_fmt);
			}
		}
	}
	if (fseek(io, 18, SEEK_SET) != 0) CLEANUP_WITH(bmp_fmt);
	uint32_t as_u32 = (uint32_t)src.width;
	if (fwrite(&as_u32, sizeof as_u32, 1, io) != 1) CLEANUP_WITH(bmp_fmt);
	as_u32 = (uint32_t)src.height;
	if (fwrite(&as_u32, sizeof as_u32, 1, io) != 1) CLEANUP_WITH(bmp_fmt);
cleanup:
	return err;
}

enum bmp_status bmp_load_from(struct image* const dest, char const* path) {
	enum bmp_status err = bmp_ok;
	FILE* const in = fopen(path, "rb");
	if (in == NULL) CLEANUP_WITH(bmp_env);
	err = bmp_load(in, dest);
cleanup:
	fclose_checked(in);
	return err;
}

enum bmp_status bmp_save_to(struct image const src, char const* path) {
	enum bmp_status err = bmp_ok;
	FILE* const io = fopen(path, "r+b");
	if (io == NULL) CLEANUP_WITH(bmp_env);
	err = bmp_save(io, src);
cleanup:
	fclose_checked(io);
	return err;
}