#pragma once

#ifdef _MSC_VER
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>

#define CLEANUP_WITH(status) do { err = status; goto cleanup; } while (0)