#include "bmp.h"
#include <stdlib.h>

int main() {
	int err = 0;
	struct image bmp = {0};
	struct image rot = {0};
	if (bmp_load_from(&bmp, "test.bmp") != bmp_ok) CLEANUP_WITH(EXIT_FAILURE);
	image_rotate(bmp, &rot);
	if (bmp_save_to(rot, "test.bmp") != bmp_ok) CLEANUP_WITH(EXIT_FAILURE);
cleanup:
	image_free(rot);
	image_free(bmp);
	return err;
}