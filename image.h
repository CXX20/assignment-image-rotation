#pragma once

#include <inttypes.h>
#include <stddef.h>

struct pixel { uint8_t r, g, b; };
struct image {
	size_t width, height;
	struct pixel* data;
};

void image_rotate(struct image this, struct image* dest);
void image_free(struct image this);