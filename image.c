#include "image.h"
#include <stdlib.h>

void image_rotate(struct image const this, struct image* const dest) {
	dest->data = malloc(sizeof(struct pixel) * this.height * this.width);
	if (dest->data == NULL) return;
	dest->width = this.height;
	dest->height = this.width;
	/*
		00 10 20 30 40 <- src
		01 11 21 31 41
		02 12 22 32 42

		02 01 00 <- dest
		12 11 10
		22 21 20
		32 31 30
		42 41 40
	*/
	for (size_t y = 0; y < this.height; ++y) {
		for (size_t x = 0; x < this.width; ++x) {
#pragma warning(push)
#pragma warning(disable: 6386) // "buffer overlow"
			dest->data[dest->width - 1 - y + x * dest->width] =
				this.data[x + y * this.width];
#pragma warning(pop)
		}
	}
}
void image_free(struct image const this) {
	free(this.data);
}