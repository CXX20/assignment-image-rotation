#pragma once

#include "util.h"
#include "image.h"

enum bmp_status {
	bmp_ok = 0,
	bmp_env,
	bmp_fmt,
	bmp_too_big,
	bmp_oom
};

enum bmp_status bmp_load(FILE* in, struct image* dest);
enum bmp_status bmp_save(FILE* io, struct image src);
enum bmp_status bmp_load_from(struct image* dest, char const* path);
enum bmp_status bmp_save_to(struct image src, char const* path);